import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { Menu, Segment } from 'semantic-ui-react'

export class Head extends Component {

    state = { activeItem: 'Home' }
    
      handleItemClick = (e, { name }) => this.setState({ activeItem: name })
    
      render() {
        const { activeItem } = this.state
    
        return (
          <Segment inverted>
              <h2>Roux Bayou</h2>
            <Menu inverted >
              <Menu.Item as={Link} to="/Home" name='Home' active={activeItem === 'Home'} onClick={this.handleItemClick} />
              <Menu.Item as={Link} to="/Menu" name='FoodMenu' active={activeItem === 'FoodMenu'} onClick={this.handleItemClick} />
              <Menu.Item as={Link} to="/Orders" name='Orders' active={activeItem === 'Orders'} onClick={this.handleItemClick} />
            </Menu>
          </Segment>
        )

    // render() {
    //     return (
    //         <div className="ui inverted segment">
    //             <h1 >Roux Bayou</h1>
    //             <div className="ui inverted secondary pointing menu">
    //                 <Link to="/Home" className="active item">Home  </Link>
    //                 <Link to="/Menu" className="item">MenuItems  </Link>
    //                 <Link to="/Orders" className="item" >Orders  </Link>
    //             </div>
    //         </div>
    //     );
    }
}