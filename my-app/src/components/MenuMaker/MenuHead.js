import React, { Component } from "react";
import { Dropdown, Menu } from 'semantic-ui-react';
// import { AddItem } from "./AddItem.js";
// import { EditItem } from "./EditItem.js";
// import { DeleteItem } from "./DeleteItem.js";

export default class MenuExampleDropdownItem extends Component {


    render() {

        return (
            <Menu vertical>
                <Dropdown item text='Edit Menu Items'>
                    <Dropdown.Menu>
                        <Dropdown.Item>Add</Dropdown.Item>
                        <Dropdown.Item>Delete</Dropdown.Item>
                        <Dropdown.Item>Edit</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </Menu>
        );
    }
}