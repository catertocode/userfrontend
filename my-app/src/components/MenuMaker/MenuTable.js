import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Form, FormGroup, FormControl, Button } from 'react-bootstrap'
import { connect } from 'react-redux'
import { Grid, Row, Col } from 'react-bootstrap'
import { Dropdown, Menu, Table, Modal, Confirm, Header } from 'semantic-ui-react';
import { Link } from "react-router-dom";
import PostAPI from "./PostAPI";


class MenuTable extends Component {
  
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this)
    this.onClear = this.onClear.bind(this)
    this.onSubmit = this.onSubmit.bind(this)

  }
  
  onChange(e) {
    if (e.target.value === "") {
      this.props.fetchData({ firstName: "menu *" })
    }
    else {
      this.props.fetchData({ firstName: e.target.value })
    }
  }

  onClear(e) {
    let searchInput = ReactDOM.findDOMNode(this.refs.searchInput)
    this.props.fetchData({ firstName: "menu *" })

  }

  onSubmit(e) {
    //prevents full page reload
    e.preventDefault();
  }

  renderTitleAndForm() {
    let titleAndForm = (
      <Grid>
        <Row className="show-grid">
          <Col smOffset={1} sm={11}> <h2> Filter Authors Database by First Name</h2></Col>
        </Row>
        <Row className="show-grid">
          <Col md={4}>
            <Form horizontal onSubmit={this.onSubmit}>
              <FormGroup controlId="formInlineEmail">
                <Col smOffset={3} sm={4}>
                  <FormControl
                    ref="searchInput"
                    type="text"
                    placeholder="Name"
                    onChange={this.onChange}
                  />
                </Col>
              </FormGroup>
            </Form>
          </Col>
        </Row>
      </Grid>
    );

    return titleAndForm;
  }

  renderFullForm() {
    let fullForm = (
      <Grid>
        <Row className="show-grid">
          <Col smOffset={1} sm={11}> <h2>Roux Bayou Catering Menu</h2></Col>
        </Row>
        <Row className="show-grid">
          <Col md={4}>
            <Form horizontal onSubmit={this.onSubmit}>
              <FormGroup controlId="formInlineEmail">
                <Col smOffset={3} sm={4}>
                  <FormControl
                    type="text"
                    placeholder="Item Name"
                    onChange={this.onChange}
                  />
                </Col>
                <Col sm={2}>
                  <Button type="button" onClick={this.onClear}>
                    Clear
                 </Button>
                </Col>
              </FormGroup>
            </Form>
          </Col>
        </Row>

        <Row className="show-grid">
          <Col smOffset={1} sm={10}>
              <BootstrapTable data={this.props.searchData}>
              <TableHeaderColumn dataField='NAME' isKey={true}>Name</TableHeaderColumn>
              <TableHeaderColumn dataField='DESCRIPTION'>Description</TableHeaderColumn>
              <TableHeaderColumn dataField='PRICE' >Price</TableHeaderColumn>
            </BootstrapTable> 
          </Col>
        </Row>
      </Grid>
        );
    return fullForm
  }

  render() {
    if (this.props.searchData.length !== 0) {
      return (
        this.renderFullForm()
      );
    } else {
      return (
        this.renderTitleAndForm()
      );
    }
  }
}


function mapStatetoProps(state) {
  return {
          searchData: state.searchData
  }
}


function mapDispatchToProps(dispatch) {
  return {
          fetchData: firstName => dispatch({type: 'FETCH_SEARCH_DATA', payload: firstName }),
  }
}

const ConnectedSearch = connect(mapStatetoProps, mapDispatchToProps)(MenuTable)

export default ConnectedSearch






// import React, {Component} from "react";
// import {Table} from 'semantic-ui-react';
// import ReactDOM from 'react-dom';

// export default class MenuTable extends Component {

//     state = {
//         menu: [],
//         lismenu: []
//     }

//     constructor(props) {
//         super(props);

//         this.onChange = this.onChange.bind(this)
//         this.onClear = this.onClear.bind(this)
//         this.onSubmit = this.onSubmit.bind(this)

//       }
//       onChange(e)
//       {
//         if(e.target.value ===""){
//           this.props.fetchData({firstName: "*"})
//         }
//         else {
//           this.props.fetchData({firstName: e.target.value})
//         }
//       }

//       onClear(e)
//       {
//         let searchInput = ReactDOM.findDOMNode(this.refs.searchInput)
//         searchInput.value=""
//         this.props.fetchData({firstName: "*"})

//       }

//       onSubmit(e)
//       {
//         //prevents full page reload
//         e.preventDefault();
//       }


//       render() {

//            if( this.props.searchData.length !== 0){
//               return (
//                 this.renderFullForm()
//               );
//             }else{
//               return (
//                 this.renderTitleAndForm()
//               );
//             }
//           }
//         }

//         function mapStatetoProps(state){
//           return {
//             searchData: state.searchData
//           }
//         }


//         function mapDispatchToProps(dispatch){
//           return {
//             fetchData: firstName => dispatch({type: 'FETCH_SEARCH_DATA', payload:firstName}),
//           }
//         }

//         const ConnectedSearch = connect(mapStatetoProps, mapDispatchToProps)(MenuTable)

//         export default ConnectedSearch;
//     render() {

//         return (
//             <div>
                // <Table celled renderBodyRow>
                //     <Table.Header >
                //         <Table.Row>
                //             <Table.HeaderCell>Item Name</Table.HeaderCell>
                //             <Table.HeaderCell>Item Description</Table.HeaderCell>
                //             <Table.HeaderCell>Item Price </Table.HeaderCell>
                //         </Table.Row>
                //     </Table.Header>

//                     <Table.Body >

//                             <Table.Row >
//                                 <Table.Cell>Spaghetti{this.props.searchData}</Table.Cell>
//                                 <Table.Cell>Made with tamato basil sauce with a pinch of sugar</Table.Cell>
//                                 <Table.Cell>$10.99</Table.Cell>
//                             </Table.Row>

//                     </Table.Body>
//                 </Table>
//             </div>
//         );
//     }
// }