import React, { Component } from 'react';
import axios from 'axios';

class PostAPI extends Component{

constructor(){
super();
this.state={
NAME:'',
DESCRIPTION:'',
PRICE:''
};
}
onChange = (e)=>{
const state = this.state
state[e.target.name] = e.target.value;
this.setState(state);
}

onSubmit = (e) =>{
e.preventDefault();
const {NAME,DESCRIPTION,PRICE} = this.state;

axios.post('http://localhost:3306/api/menu',{
NAME,DESCRIPTION,PRICE,
headers : {
"Content-Type": "application/json",
"Access-Control-Allow-Origin" : "http://localhost:3306/api/menu", 
"Access-Control-Allow-Credentials" : true
} 
})
.then((result)=>{

});
}
render(){
const {NAME,DESCRIPTION,PRICE}= this.state;
return(
<form onSubmit={this.onSubmit}>
<input type="text" placeholder="Name" name="menuType" value={NAME} onChange={this.onChange}/><br/>
<input type="text" placeholder="Descriptions" name="menuName" value={DESCRIPTION} onChange={this.onChange}/><br/>
<input type="text" placeholder="Price" name="menuPrice" value={PRICE} onChange={this.onChange}/><br/>
<button type= "submit" >Create</button>
</form>
);
}
}

export default PostAPI;