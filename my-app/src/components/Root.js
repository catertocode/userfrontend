import React from "react";
import { Head } from "./Head";
import "../css/Root.css";

export class Root extends React.Component {
    render() {
        return (
            <div >
                <Head/>
                <div>
                    <div>
                        {this.props.children}
                    </div>
                </div>

            </div>
        );
    }
}