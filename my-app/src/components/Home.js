import React from "react";
import {Table} from 'semantic-ui-react';
import "../css/Home.css";
export class Home extends React.Component {
    render() {

        const colors = [
            'tan'
        ]
        const nah = [
            'blue'
        ]


        return (
            <div>
                <h1>Upcoming Events</h1>

                {colors.map(color => (
                <Table color={color} key={color} celled inverted>
                    <Table.Header>    
                    <Table.Row > 
                        <Table.HeaderCell >Date</Table.HeaderCell>
                        <Table.HeaderCell >Event</Table.HeaderCell>
                        <Table.HeaderCell >Client </Table.HeaderCell>
                        </Table.Row>
                
                    </Table.Header>
                <Table.Body>
                    <Table.Row>
                        <Table.Cell>October 30, 2017</Table.Cell>
                        <Table.Cell>Raymonds 5th birthday party</Table.Cell>
                        <Table.Cell>Raymond Ramon</Table.Cell>
                        </Table.Row>
                    <Table.Row>
                        <Table.Cell>October 31, 2017</Table.Cell>
                        <Table.Cell>SELU Faculty Halloween</Table.Cell>
                        <Table.Cell>Southeastern University</Table.Cell>
                        </Table.Row>
                    <Table.Row>
                        <Table.Cell>November 24, 2017</Table.Cell>
                        <Table.Cell>Rec Center Thanksgiving Feast</Table.Cell>
                        <Table.Cell>Rec Connect Club</Table.Cell>
                        </Table.Row>
                    <Table.Row>
                        <Table.Cell>November 26, 2017</Table.Cell>
                        <Table.Cell>SELU baseball faculty and staff lunch</Table.Cell>
                        <Table.Cell>Coach Armstrong</Table.Cell>
                        </Table.Row>
                </Table.Body>
                 </Table>
                ))}

                <h1>New Orders</h1>
                {nah.map(color => (
                 <Table color={color} key={color} celled inverted>
                    <Table.Header>    
                    <Table.Row > 
                        <Table.HeaderCell >Date</Table.HeaderCell>
                        <Table.HeaderCell >Event</Table.HeaderCell>
                        <Table.HeaderCell >Client </Table.HeaderCell>
                        </Table.Row>
                
                    </Table.Header>
                <Table.Body>
                    <Table.Row>
                        <Table.Cell>December 1, 2017</Table.Cell>
                        <Table.Cell>Nancy's 103rd birthday get down</Table.Cell>
                        <Table.Cell>Francis Pliers</Table.Cell>
                        </Table.Row>
                    <Table.Row>
                        <Table.Cell>December 16, 2017</Table.Cell>
                        <Table.Cell>Hammond Highschool Winter Formal</Table.Cell>
                        <Table.Cell>Hammond High</Table.Cell>
                        </Table.Row>
                    <Table.Row>
                        <Table.Cell>December 21, 2017</Table.Cell>
                        <Table.Cell>Ponchatoula High Winter Formal</Table.Cell>
                        <Table.Cell>Ponchatoula High</Table.Cell>
                        </Table.Row>
                    <Table.Row>
                        <Table.Cell>December 25, 2017</Table.Cell>
                        <Table.Cell>Sports Academy Annual Christmas Party</Table.Cell>
                        <Table.Cell>Manager Chris Franklin</Table.Cell>
                        </Table.Row>
                </Table.Body>
                 </Table>

                 
            ))}
            </div>
        );
    }
}