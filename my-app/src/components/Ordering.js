import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Form, FormGroup, FormControl, Button } from 'react-bootstrap'
import { connect } from 'react-redux'
import { Grid, Segment } from 'semantic-ui-react';
import { Dropdown, Menu, Table, Modal, Confirm, Header } from 'semantic-ui-react';
import { Link } from "react-router-dom";


class Ordering extends Component {


  state = { activeItem: 'All Orders' }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  renderTitleAndForm() {
    let titleAndForm = (
      <Grid>
        <Grid.Column width={4}>
          <Menu fluid vertical tabular>
            <Menu.Item name='New Orders' active={this.state.activeItem === 'New Orders'} onClick={this.handleItemClick} />
            <Menu.Item name='Accepted Orders' active={this.state.activeItem === 'Accepted Orders'} onClick={this.handleItemClick} />
            <Menu.Item name='Denied Orders' active={this.state.activeItem === 'Denied Orders'} onClick={this.handleItemClick} />
          </Menu>
        </Grid.Column>
      </Grid>
    );

    return titleAndForm;
  }
   
  renderFullForm() {  
    this.props.fetchData({ firstName: "orders *" })
    let fullForm = (
      <Grid>
        <Grid.Column width={4}>
          <Menu fluid vertical tabular>
          <Menu.Item name='All Orders' active={this.state.activeItem === 'All Orders'} onClick={this.handleItemClick} />
            <Menu.Item name='New Orders' active={this.state.activeItem === 'New Orders'} onClick={this.handleItemClick} />
            <Menu.Item name='Accepted Orders' active={this.state.activeItem === 'Accepted Orders'} onClick={this.handleItemClick} />
            <Menu.Item name='Denied Orders' active={this.state.activeItem === 'Denied Orders'} onClick={this.handleItemClick} />
          </Menu>
        </Grid.Column>

        <Grid.Column stretched width={12}>

            <BootstrapTable data={this.props.searchData} search={false}>
            <TableHeaderColumn dataField='ID' isKey={true}>ID</TableHeaderColumn>
              <TableHeaderColumn dataField='Customer'>Customer</TableHeaderColumn>
              <TableHeaderColumn dataField='Date'>Date</TableHeaderColumn>
              <TableHeaderColumn dataField='Price'>Price</TableHeaderColumn>
            </BootstrapTable>
            {/* <OrderChange name ={this.state.activeItem} /> */}
 

        </Grid.Column>
      </Grid>
    );
    return fullForm
  }

  render() {
    const { activeItem } = this.state;

    if (this.props.searchData.length !== 0) {
      return (
        this.renderFullForm()
      );
    } else {
      return (
        this.renderTitleAndForm()
      );
    }
  }
}

function mapStatetoProps(state) {
  return {
    searchData: state.searchData
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchData: firstName => dispatch({ type: 'FETCH_SEARCH_DATA', payload: firstName }),
  }
}

// export class OrderChange extends Ordering {
//   render() {
//     if (this.props.name === "New Orders") {
//       return (
//         <BootstrapTable data={this.props.searchData} search={false}>
//           <TableHeaderColumn dataField='NAME' isKey={true}>Name</TableHeaderColumn>
//           <TableHeaderColumn dataField='DESCRIPTION'>Description</TableHeaderColumn>
//           <TableHeaderColumn dataField='PRICE' >Price</TableHeaderColumn>
//         </BootstrapTable>
//       );
//     } else {
//       if (this.props.name === "Accepted Orders") {
//         return (
//           <BootstrapTable data={this.props.searchData} search={false}>
//             <TableHeaderColumn dataField='NAME' isKey={true}>Name</TableHeaderColumn>
//             <TableHeaderColumn dataField='DESCRIPTION'>Description</TableHeaderColumn>
//             <TableHeaderColumn dataField='PRICE' >Price</TableHeaderColumn>
//           </BootstrapTable>
//         );

//       } else {
//         if (this.props.name === "Denied Orders") {
//           return (
//             <BootstrapTable data={this.props.searchData} search={false}>
//               <TableHeaderColumn dataField='NAME' isKey={true}>Name</TableHeaderColumn>
//               <TableHeaderColumn dataField='DESCRIPTION'>Description</TableHeaderColumn>
//               <TableHeaderColumn dataField='PRICE' >Price</TableHeaderColumn>
//             </BootstrapTable>
//           );
//         } else {
//           return (
//             <tr>Sorry Not Working</tr>
//           );
//         }
//       }   
//     }

//   }
// }

const ConnectedSearch = connect(mapStatetoProps, mapDispatchToProps)(Ordering)

export default ConnectedSearch




// import React, { Component } from 'react';
// import { Menu } from 'semantic-ui-react';
// import { Table } from 'semantic-ui-react';
// import { Grid, Segment } from 'semantic-ui-react';
// import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
// import ReactDOM from 'react-dom'
// import { connect } from 'react-redux'

// export class Ordering extends Component {
//   state = { activeItem: 'New Orders' }

//   handleItemClick = (e, { name }) => this.setState({ activeItem: name })



//   render() {
//     const { activeItem } = this.state



//     return (

      // <Grid>
      //   <Grid.Column width={4}>
      //     <Menu fluid vertical tabular>
      //       <Menu.Item name='New Orders' active={activeItem === 'New Orders'} onClick={this.handleItemClick} />
      //       <Menu.Item name='Accepted Orders' active={activeItem === 'Accepted Orders'} onClick={this.handleItemClick} />
      //       <Menu.Item name='Denied Orders' active={activeItem === 'Denied Orders'} onClick={this.handleItemClick} />
      //     </Menu>
      //   </Grid.Column>

      //   <Grid.Column stretched width={12}>
      //     <Segment>
      //       <Table celled>

      //         <OrderChange name={this.state.activeItem} />
      //       </Table>
      //     </Segment>
      //   </Grid.Column>
      // </Grid>
//     )
//   }
// }

// export class OrderChange extends Ordering {
//   render() {
//     if (this.props.name === "New Orders") {
//       return (
//         <BootstrapTable data={this.props.searchData} search={false}>
//           <TableHeaderColumn dataField='NAME' isKey={true}>Name</TableHeaderColumn>
//           <TableHeaderColumn dataField='DESCRIPTION'>Description</TableHeaderColumn>
//           <TableHeaderColumn dataField='PRICE' >Price</TableHeaderColumn>
//         </BootstrapTable>
//       );
//     }
//     if (this.props.name === "Accepted Orders") {
//       return (
//         <BootstrapTable data={this.props.searchData} search={false}>
//           <TableHeaderColumn dataField='NAME' isKey={true}>Name</TableHeaderColumn>
//           <TableHeaderColumn dataField='DESCRIPTION'>Description</TableHeaderColumn>
//           <TableHeaderColumn dataField='PRICE' >Price</TableHeaderColumn>
//         </BootstrapTable>
//       );

//     }
//     if (this.props.name === "Denied Orders") {
//       return (
//         <BootstrapTable data={this.props.searchData} search={false}>
//           <TableHeaderColumn dataField='NAME' isKey={true}>Name</TableHeaderColumn>
//           <TableHeaderColumn dataField='DESCRIPTION'>Description</TableHeaderColumn>
//           <TableHeaderColumn dataField='PRICE' >Price</TableHeaderColumn>
//         </BootstrapTable>
//       );

//     }

//   }
// }

// function mapStatetoProps(state) {
//   return {
//     searchData: state.searchData
//   }
// }


// function mapDispatchToProps(dispatch) {
//   return {
//     fetchData: firstName => dispatch({ type: 'FETCH_SEARCH_DATA', payload: firstName }),
//   }
// }

// const ConnectedSearch = connect(mapStatetoProps, mapDispatchToProps)(Ordering)

// export default ConnectedSearch
