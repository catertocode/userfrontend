import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import mainReducer from './Reducers'
import watchFetchSearchData from './Sagas.js'

import React from 'react';
import { Loging } from "./components/Loging";
import { Root } from "./components/Root";
import { Menu } from "./components/MenuMaker/Menu";
import { Orders } from "./components/Orders";
import { Home } from "./components/Home";
import { ConnectedSearch } from "./components/Search";
import { BrowserRouter, Route} from "react-router-dom";
import 'semantic-ui-css/semantic.min.css';

//saga middleware
const sagaMiddleware = createSagaMiddleware()

//redux store with saga middleware
const store = createStore(
    mainReducer,
    applyMiddleware(sagaMiddleware)
)
// activate the saga(s)
sagaMiddleware.run(watchFetchSearchData)

//fetch initial data
store.dispatch({ type: 'FETCH_SEARCH_DATA', payload: { firstName: "menu *" } })


ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <div>
                <Route path="/login" exact component={Loging} />
                <Root>
                    <Route path="/Home" exact component={Home} />
                    <Route path="/Menu" exact component={Menu} />
                    <Route path="/Orders" exact component={Orders}/>
                </Root>
            </div>
        </BrowserRouter>
    </Provider>,
    window.document.getElementById('root'));